# [RDF4J](https://rdf4j.org) Activitystreams Vocabulary

Activitystreams constants for RDF primitives.  
Generated with https://github.com/ansell/rdf4j-schema-generator using the owl file: https://www.w3.org/ns/activitystreams-owl

## Usage
```
<dependency>
    <groupId>de.naturzukunft.rdf.rdf4j-vocabulary</groupId>
    <artifactId>activitystreams</artifactId>
    <version>1.2</version>
</dependency>    
```
